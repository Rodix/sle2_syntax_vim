" Vim syntax file
" Language: sle2
" Maintainer: Rodrigo Rodi <rodicr@gmail.com>
" Last Change: 2014 Mar 17 by Rodrigo Rodi


syn keyword sle2Statement inicio fin imprimir programa const var matriz
syn keyword sle2Statement FALSE TRUE subrutina not and
syn match   sle2MathsOperator   "-\|=\|[:<>+%\*^/\\]\|AND\|OR"
syn keyword sle2Conditional si sino eval caso
syn keyword sle2Repeat  mientras desde hasta repetir
syn keyword sle2TypeSpecifier numerico logico cadena
syn match sle2Comment "//.*$"
syn region sle2CommentString start=+/\*+ end=+\*/+
syn region sle2CommentLine start=+"+ end=+"+
syn match sle2Parent contained "()"
"enteros o comas flotantes sin punto
syn match   sle2Number      "\<0[oO]\=\o\+[Ll]\=\>"
syn match   sle2Number      "\<0[xX]\x\+[Ll]\=\>"
syn match   sle2Number      "\<0[bB][01]\+[Ll]\=\>"
syn match   sle2Number      "\<\%([1-9]\d*\|0\)[Ll]\=\>"
syn match   sle2Number      "\<\d\+[jJ]\>"
syn match   sle2Number      "\<\d\+[eE][+-]\=\d\+[jJ]\=\>"
syn match   sle2Number
        \ "\<\d\+\.\%([eE][+-]\=\d\+\)\=[jJ]\=\%(\W\|$\)\@="
syn match   sle2Number
        \ "\%(^\|\W\)\@<=\d*\.\d\+\%([eE][+-]\=\d\+\)\=[jJ]\=\>"

command -nargs=+ HiLink hi def link <args>

HiLink sle2Statement Statement
HiLink sle2Conditional Conditional
HiLink sle2Repeat Repeat
HiLink sle2TypeSpecifier Type
HiLink sle2Comment Comment
HiLink sle2CommentString Comment
HiLink sle2MathsOperator Operator
HiLink sle2CommentLine String
HiLink sle2Parent Parent
HiLink sle2Number Number
" vim:set sw=2 sts=2 ts=8 noet:
